# Copyright 2019 Schicksal
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Aphora.Worker do
  @moduledoc """
  `Aphora.Worker` implements a `GenServer`, which can generate guaranteed unique `t:Aphora.id/0` on demand.
  """

  use GenServer

  @datacenter_range 0..31
  @worker_range 0..1_023
  @timestamp_range 0..35_184_372_088_831
  @counter_overflow 4_096
  @counter_range 0..(@counter_overflow - 1)

  def start_link(%{epoch: epoch, datacenter: datacenter, worker: worker} = state)
      when datacenter in @datacenter_range and is_integer(datacenter) and
             worker in @worker_range and is_integer(worker) do
    state = state |> Map.put(:timestamp, timestamp(epoch)) |> Map.put(:counter, 0)
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  def init(state), do: {:ok, state}

  def handle_call(:new_id, from, state) do
    case valid_state?(state) do
      {:ok, state} ->
        {:reply, {:ok, build_id(state)}, state}

      {:error, :counter_overflow} ->
        Process.sleep(1)
        handle_call(:new_id, from, state)

      {:error, :reverted_tick} ->
        {:reply, {:error, :reverted_tick}, state}
    end
  end

  defp valid_state?(%{epoch: epoch, timestamp: timestamp} = state) do
    case timestamp(epoch) do
      ^timestamp -> old_timestamp(state)
      new_timestamp -> new_timestamp(state, new_timestamp)
    end
  end

  defp old_timestamp(%{counter: counter} = state) do
    case counter + 1 do
      @counter_overflow -> {:error, :counter_overflow}
      new_counter -> {:ok, state |> Map.put(:counter, new_counter)}
    end
  end

  defp new_timestamp(%{timestamp: timestamp} = state, new_timestamp) do
    if new_timestamp < timestamp do
      {:error, :reverted_tick}
    else
      {:ok, state |> Map.put(:timestamp, new_timestamp) |> Map.put(:counter, 0)}
    end
  end

  @doc """
  Returns the unique `t:Aphora.id/0`.

  It first combines the binary representation of the information within the state into a `72 Bits` binary.  
  Afterwards it gets encoded using `Aphora.Helper.encode/1`, so it can be easily used for any `URI`.

  ## Examples

      iex> Aphora.Worker.build_id(%{datacenter: 2, worker: 423, timestamp: 912_988_800_000, counter: 1207})
      "0eHHz1--abHr"
  """
  @doc since: "0.1.0"
  @spec build_id(%{
          datacenter: Aphora.datacenter(),
          worker: Aphora.worker(),
          timestamp: Aphora.timestamp(),
          counter: Aphora.counter()
        }) ::
          Aphora.id()
  def build_id(
        %{datacenter: datacenter, worker: worker, timestamp: timestamp, counter: counter} = _state
      )
      when datacenter in @datacenter_range and is_integer(datacenter) and
             worker in @worker_range and is_integer(worker) and
             timestamp in @timestamp_range and is_integer(timestamp) and
             counter in @counter_range and is_integer(counter) do
    <<
      timestamp::integer-size(45),
      datacenter::integer-size(5),
      worker::integer-size(10),
      counter::integer-size(12)
    >>
    |> Aphora.Helper.encode()
  end

  @doc """
  Returns the `t:Aphora.timestamp/0`.

  It gets calculated by taking the current time `System.os_time/1` and substracting the `t:Aphora.epoch/0` of it.

  ## Examples

      iex> Aphora.Worker.timestamp(0) == System.os_time(:millisecond)
      true
  """
  @doc since: "0.1.0"
  @spec timestamp(Aphora.epoch()) :: Aphora.timestamp()
  def timestamp(epoch) when 0 <= epoch and is_integer(epoch),
    do: System.os_time(:millisecond) - epoch
end
