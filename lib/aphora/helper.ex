# Copyright 2019 Schicksal
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Aphora.Helper do
  @moduledoc """
  `Aphora.Helper` provides many utility methods, which are meant to facilitate the handling and encoding of `t:Aphora.id/0`.
  """

  @encoding_alphabet %{
    0 => "-",
    1 => "0",
    2 => "1",
    3 => "2",
    4 => "3",
    5 => "4",
    6 => "5",
    7 => "6",
    8 => "7",
    9 => "8",
    10 => "9",
    11 => "A",
    12 => "B",
    13 => "C",
    14 => "D",
    15 => "E",
    16 => "F",
    17 => "G",
    18 => "H",
    19 => "I",
    20 => "J",
    21 => "K",
    22 => "L",
    23 => "M",
    24 => "N",
    25 => "O",
    26 => "P",
    27 => "Q",
    28 => "R",
    29 => "S",
    30 => "T",
    31 => "U",
    32 => "V",
    33 => "W",
    34 => "X",
    35 => "Y",
    36 => "Z",
    37 => "_",
    38 => "a",
    39 => "b",
    40 => "c",
    41 => "d",
    42 => "e",
    43 => "f",
    44 => "g",
    45 => "h",
    46 => "i",
    47 => "j",
    48 => "k",
    49 => "l",
    50 => "m",
    51 => "n",
    52 => "o",
    53 => "p",
    54 => "q",
    55 => "r",
    56 => "s",
    57 => "t",
    58 => "u",
    59 => "v",
    60 => "w",
    61 => "x",
    62 => "y",
    63 => "z"
  }

  @doc """
  Encodes the `t:binary/0` into a `t:Aphora.id/0`.

  It uses a custom alphabet inspired by [Firebase's push ID](https://firebase.googleblog.com/2015/02/the-2120-ways-to-ensure-unique_68.html)

  *`encode/1` completely ignores the usual Base64 padding. This is absolutely fine in case of an `t:Aphora.id/0`, because its size of `72 Bit` is divisible by `24`.  
  Make sure to **not** encode anything, which isn't exactly divisible by `24`, otherwise you'll lose data!*

  ## Examples

      iex> Aphora.Helper.encode(<<6, 164, 146, 252, 32, 0, 154, 116, 183>>)
      "0eHHz1--abHr"
  """
  @doc since: "0.2.0"
  @spec encode(<<_::72>>) :: Aphora.id()
  def encode(binary) do
    grouped = for <<character::6 <- binary>>, do: character
    encoded = for character <- grouped, do: Map.fetch!(@encoding_alphabet, character)
    Enum.join(encoded)
  end

  @decoding_alphabet %{
    "-" => 0,
    "0" => 1,
    "1" => 2,
    "2" => 3,
    "3" => 4,
    "4" => 5,
    "5" => 6,
    "6" => 7,
    "7" => 8,
    "8" => 9,
    "9" => 10,
    "A" => 11,
    "B" => 12,
    "C" => 13,
    "D" => 14,
    "E" => 15,
    "F" => 16,
    "G" => 17,
    "H" => 18,
    "I" => 19,
    "J" => 20,
    "K" => 21,
    "L" => 22,
    "M" => 23,
    "N" => 24,
    "O" => 25,
    "P" => 26,
    "Q" => 27,
    "R" => 28,
    "S" => 29,
    "T" => 30,
    "U" => 31,
    "V" => 32,
    "W" => 33,
    "X" => 34,
    "Y" => 35,
    "Z" => 36,
    "_" => 37,
    "a" => 38,
    "b" => 39,
    "c" => 40,
    "d" => 41,
    "e" => 42,
    "f" => 43,
    "g" => 44,
    "h" => 45,
    "i" => 46,
    "j" => 47,
    "k" => 48,
    "l" => 49,
    "m" => 50,
    "n" => 51,
    "o" => 52,
    "p" => 53,
    "q" => 54,
    "r" => 55,
    "s" => 56,
    "t" => 57,
    "u" => 58,
    "v" => 59,
    "w" => 60,
    "x" => 61,
    "y" => 62,
    "z" => 63
  }

  @doc """
  Decodes the `t:Aphora.id/0` into a `t:binary/0`.

  It basically is just a reverse implementation of `encode/1`, so please ensure to read the warnings of it.

  ## Examples

      iex> Aphora.Helper.decode("0eHHz1--abHr")
      <<6, 164, 146, 252, 32, 0, 154, 116, 183>>
  """
  @doc since: "0.2.0"
  @spec decode(Aphora.id()) :: <<_::72>>
  def decode(id) do
    encoded = String.graphemes(id)
    grouped = for character <- encoded, do: Map.fetch!(@decoding_alphabet, character)
    Enum.into(grouped, <<>>, fn byte -> <<byte::6>> end)
  end

  @doc """
  Extracts the `t:Aphora.timestamp/0` out of the given `t:Aphora.id/0`.

  If you select `:absolute`, `get_timestamp/2` will calculate the actual `t:Aphora.timestamp/0`.  
  If you instead want a `t:Aphora.timestamp/0` relative to your `t:Aphora.epoch/0`, use `:relative`.

  ## Examples

      iex> Aphora.Helper.get_timestamp("0eHHz1--abHr", :relative)
      912_988_800_000

      iex> Aphora.Helper.get_timestamp("-0dqbzfF----", :absolute)
      1_560_508_218_202
  """
  @doc since: "0.4.0"
  @spec get_timestamp(Aphora.id(), :relative) :: Aphora.timestamp()
  def get_timestamp(id, :relative) do
    <<
      timestamp::integer-size(45),
      _datacenter::integer-size(5),
      _worker::integer-size(10),
      _counter::integer-size(12)
    >> = decode(id)

    timestamp
  end

  @doc since: "0.4.0"
  @spec get_timestamp(Aphora.id(), :absolute) :: Aphora.timestamp()
  def get_timestamp(id, :absolute) do
    get_timestamp(id, :relative) + Aphora.Config.get_epoch()
  end
end
