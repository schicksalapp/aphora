# Copyright 2019 Schicksal
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Aphora.Config do
  @moduledoc """
  `Aphora.Config` provides a simple interface to the `config.exs` file, by providing a `default` variable fallback.
  """

  @default [
    datacenter: 0,
    worker: 0,
    epoch: 1_546_300_800_000
  ]

  @doc """
  Returns the set unique `t:Aphora.datacenter/0` identifier from the config, or otherwise the `default` value `0`.  

  ## Examples

      iex> Aphora.Config.get_datacenter()
      0
  """
  @doc since: "0.1.0"
  @spec get_datacenter() :: Aphora.datacenter()
  def get_datacenter, do: Application.get_env(:aphora, :datacenter) || @default[:datacenter]

  @doc """
  Returns the set unique `t:Aphora.epoch/0` identifier from the config, or otherwise the `default` value `1_546_300_800_000`.  

  ## Examples

      iex> Aphora.Config.get_epoch()
      1_546_300_800_000
  """
  @doc since: "0.1.0"
  @spec get_epoch() :: Aphora.epoch()
  def get_epoch, do: Application.get_env(:aphora, :epoch) || @default[:epoch]

  @doc """
  Returns the set unique `t:Aphora.worker/0` identifier from the config, or otherwise the `default` value `0`.  

  ## Examples

      iex> Aphora.Config.get_worker()
      0
  """
  @doc since: "0.1.0"
  @spec get_worker() :: Aphora.worker()
  def get_worker, do: Application.get_env(:aphora, :worker) || @default[:worker]
end
