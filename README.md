# Aphora
[![Build Status](https://travis-ci.org/schicksalapp/aphora.svg?branch=master)](https://travis-ci.org/schicksalapp/aphora)
[![Hex.pm Version](https://img.shields.io/hexpm/v/aphora.svg?style=flat)](https://hex.pm/packages/aphora)

Aphora is a system do distributedly generate unique `id` without any coordination. It is similiar to [Snowflake](https://github.com/twitter-archive/snowflake/tree/snowflake-2010), but instead of an integer it generates ASCII sortable a String.

It uses a `72 Bit` long Binary which get encoded to 12 characters long ID.  
An ID is composed of:
* `45 Bit` Timestamp with millisecond precision. It gives up to 1115 years with a custom epoch.
* `5 Bit` Datacenter Identifier, which allows up to `32` different datacenters.
* `10 Bit` Worker Identifier, which allows up to `1024` different processes/workers/machines within a datacenter.
* `12 Bit` Counter, which allows for `4096` unique IDs to be generated each millisecond. Aphora also protects against rollovers to happen within the same millisecond.

Just like [Snowflake](https://github.com/twitter-archive/snowflake/tree/snowflake-2010), Aphora protects from non-monotonic clocks, by refusing to generate timestamp until it gets provided with a time equals or after the one used in the last generated ID.

## Installation

The package can be installed by adding `:aphora` to your list of dependencies in `mix.exs`:

```elixir
def deps do
[
  {:aphora, "~> 1.0.0"}
]
end
```

The config file is located at `/config/config.exs`. If you run `Aphora` in more than one process, you'll need to adjust the `:worker` and `:datacenter`.
```elixir
config :aphora,
# Value between 0..31, unique between all `t:datacenter/0`.
datacenter: 0,
# Value between 0..1_024, unique between all `t:worker/0` within a `t:datacenter/0`.
worker: 0,
# Shouldn't be changed once the first `t:id/0` is generated. Default: 2019-01-01 00:00:00.000000Z in milliseconds.
epoch: 1_546_300_800_000
```

The docs can be found at [Hex](https://hexdocs.pm/aphora).

## Usage

The usage of `Aphora` is quite straightforward after setting up the `config.exs` correctly, as all important methods are within the `Aphora` module.  
To generate a new ID, you use `Aphora.new_id/0`.
```elixir
Aphora.new_id()
# {:ok, "0eHHz1--abHr"}
```

And if you want to find out when a ID was generated, you use `Aphora.to_timestamp/2`.
```elixir
Aphora.to_timestamp("0eHHz1--abHr", :relative)
# 912988800000
```

The docs can be found at [Hex](https://hexdocs.pm/aphora).

## License

Aphora is licensed under the [Apache License 2.0](LICENSE.md).