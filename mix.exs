# Copyright 2019 Schicksal
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Aphora.MixProject do
  use Mix.Project

  def project do
    [
      app: :aphora,
      version: "1.0.0",
      elixir: "~> 1.8",
      start_permanent: Mix.env() == :prod,
      package: package(),
      deps: deps(),
      name: "Aphora",
      docs: [
        main: "Aphora"
      ],
      source_url: "https://github.com/schicksalapp/aphora",
      homepage_url: "https://schicksal.app"
    ]
  end

  defp package do
    [
      description: "A secure and unique ID system for distributed workloads.",
      licenses: ["Apache-2.0"],
      links: %{
        "GitHub" => "https://github.com/schicksalapp/aphora"
      }
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {Aphora, []}
    ]
  end

  defp deps do
    [
      {:ex_doc, "~> 0.19", only: :dev, runtime: false},
      {:credo, "~> 1.1.0", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0.0-rc.6", only: [:dev], runtime: false}
    ]
  end
end
